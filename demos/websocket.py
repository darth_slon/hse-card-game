import asyncio
import logging
import json
import contextlib
from aiohttp import web, websocket

loop = asyncio.get_event_loop()
log = logging.getLogger("game")

GAMES={}

def start_round(clients):
    log.info("Round started")
    for c in clients:
        c.send_str("start")

async def echo_ws_handler(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)

    game_id = request.match_info["game_id"]
    if game_id in GAMES:
        GAMES[game_id]["clients"].append(ws) # join game
        start_round(GAMES[game_id]["clients"])
        player_number = 1
        log.info("Second player joined")
    else:
        GAMES[game_id] = {"clients": [ws],
                          "moves": [None, None]} # start new game
        player_number = 0
        log.info("First player joined")

    game = GAMES[game_id]
    moves = game["moves"]

    async for msg in ws:
        if msg.tp == websocket.MSG_TEXT:
            log.info("Received msg %s", str(msg))            
            moves[player_number] = msg.data

            if all(moves):
                first_win = {("paper", "rock"),
                             ("rock", "scissors"),
                             ("scissors", "paper")}
                if moves[0] == moves[1]:
                    round_result = "draw"
                elif (moves[0], moves[1]) in first_win:
                    round_result = "first"
                else:
                    round_result = "second"

                game["moves"][:] = [None, None]

                for c in game["clients"]:
                    c.send_str(round_result)

                log.info("Round finished: %s", round_result)

                await asyncio.sleep(2.0)

                start_round(game["clients"])
        elif msg.tp == websocket.MSG_CLOSE:
            log.info("Error: %s", str(msg))
            break
        else:
            log.info("Strange msg: %s", str(msg))

    log.info("Closing websocket connection")
    return ws


async def start_server():
    app = web.Application()
    app.router.add_route("GET", "/game/{game_id}", echo_ws_handler)
    server = await loop.create_server(app.make_handler(), '0.0.0.0', 8081)
    return server

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, format="%(asctime)s [%(levelname)s]: %(message)s")
    log.info("Starting server")

    loop.run_until_complete(start_server())
    loop.run_forever()
